package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
)

type Page struct {
	Title string
	Body  []byte
}

func (p *Page) save() error {
	filename := p.Title + ".txt"
	return ioutil.WriteFile(filename, p.Body, 0600)
}

func loadPage(title string) (*Page, error) {
	filename := title + ".txt"
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return &Page{Title: title, Body: body}, nil
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	otsik, _ := reader.ReadString('\n')
	tekst, _ := reader.ReadString('\n')
	p1 := &Page{Title: otsik, Body: []byte(tekst)}
	p1.save()
	p2, _ := loadPage("TestPage")
	fmt.Println(string(p2.Body))
}
Akseli Kantola
